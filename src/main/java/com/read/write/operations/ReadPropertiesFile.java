package com.read.write.operations;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
/**
 * This java file is used to read the properties file from the resource folder. 
 * @author Mayank
 *
 */
public class ReadPropertiesFile {

	public static void main(String[] args) {
		try {
			InputStream input = new FileInputStream("src/main/resources/config.properties");
			
			Properties properties = new Properties();
			
			//load a properties file from class path, inside static method
			properties.load(input);
			
			System.out.println("Username: " + properties.getProperty("db.user", StringUtils.EMPTY));
			System.out.println("Password: " + properties.getProperty("db.password", StringUtils.EMPTY));

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
