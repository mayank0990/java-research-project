package com.org.java;

import org.apache.commons.lang3.StringUtils;

public class ContainsCheck {
	
	public static void main(String[] args) {
		String str = "/content/dam/indusind/excel/Website_BranchList.xlsx";
		System.out.println(StringUtils.containsIgnoreCase(str, "branch"));
		
		String str2 = "a";
		if(str2 == "a") 
			str2 = null;
		if(StringUtils.containsIgnoreCase(str, "branch") && str2 != null) {
			System.out.println("if block!");
			System.out.println(str2.toLowerCase());
		} else {
			System.out.println("else block!");
		}
		
		String newStr = "Loidapada, AT/PO - Guali, Dist - Keonjhar - 758 035 , Orissa";
		System.out.println(newStr.length());
	}

}
