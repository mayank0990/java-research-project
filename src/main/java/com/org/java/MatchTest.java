package com.org.java;

import org.apache.commons.lang3.math.NumberUtils;

public class MatchTest {
	
	public static void main(String[] args) {
		String str = "34";
		String str1 = "3435";
		
		if(NumberUtils.isParsable(str) && NumberUtils.isParsable(str1)) {
			double total = Double.parseDouble(str) + Double.parseDouble(str1);
			System.out.println(total);
		}
		
	}

}
