package com.org.java;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class RangeTest {
	
	public static void main(String[] args) {
		int pageSize = 3;
		int totalCount = 12;
		
		int count = 1;
		int val = 0;
		for(int i=0; i<totalCount; i++) {
			val = pageSize*count;
			count++;			
			if(val > totalCount || val == totalCount) {
				break;
			}
		}
		List<Integer> strRange = new ArrayList<>();
		IntStream.range(1, count).forEach(strRange::add);
		
	}
	
	

}
