package com.org.java;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ContainsTest {
	
	public static void main(String[] args) {
		List<String> fYears = new LinkedList<>(Arrays.asList("FY 2015", "FY 2018", "FY 2017", "FY 2019" , "FY 2016"));
		List<String> qYears = new LinkedList<>(Arrays.asList("Q1 2017", "Q2 2017", "Q3 2017" , "Q1 2016", "Q2 2016", "Q4 2016"));
		int fYearsSize = fYears.size();
		int qYearsSize = qYears.size();
		
		Collections.sort(fYears, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return extractInt(o1) - extractInt(o2);
			}
			
			int extractInt(String s) {
				String num;
				if(s.contains("FY")) {
					num = s.replaceAll("\\D", "");
				} else {
					num = s.split("\\s")[1];
				}
	            return num.isEmpty() ? 0 : Integer.parseInt(num);
	        }
		});
			
		System.out.println(fYears);
		System.out.println(fYears.subList(fYearsSize-4, fYearsSize));
	
		System.out.println(qYears);
		System.out.println(qYears.subList(qYearsSize-4, qYearsSize));
	}
}
