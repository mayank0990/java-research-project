package com.org.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class NullCheck {
	
	public static void main(String[] args) {
		
		Employee obj1 = null;
		
		Employee obj2 = new Employee();
		obj2.setName(null);
		
		
		Employee obj3 = new Employee();
		obj3.setName("Hemang");
		obj3.setId(12);
		
		List<Employee> list = new ArrayList<>();
		
		Collections.addAll(list, obj1, obj2, obj3);
					
		list.forEach(item -> {			
			Optional<Employee> employee = Optional.ofNullable(item);			
			
			String name = employee.map(Employee::getName).orElse("No employee found");
			Integer id = employee.map(Employee::getId).orElse(-1);
			System.out.println(name.toLowerCase());
			System.out.println(id);
			
			
			System.out.println("Only Present names");
			employee
				.map(Employee::getName)
				.ifPresent(System.out::println);			
		});
		
		
	}
}
