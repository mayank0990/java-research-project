package com.org.java;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SortList {
	
	public static void main(String[] args) {
		List<Integer> years = new LinkedList<>();
		years.add(2015);
		years.add(2019);
		years.add(2017);
		years.add(2012);
		Collections.sort(years);
		System.out.println(years);
	}

}
