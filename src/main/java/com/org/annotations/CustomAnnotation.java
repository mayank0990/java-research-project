package com.org.annotations;

import com.org.java.Employee;

@MyModel(adaptables = {Employee.class})
public class CustomAnnotation<ModelType> {
	
	private final Class<ModelType> type;
	
	public CustomAnnotation(Class<ModelType> type) {
        this.type = type;
    }
	
}
