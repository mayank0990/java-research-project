package com.org.java8.features;

@FunctionalInterface
public interface FuncInterface {
	
	public void sum(int x);
	
	public default void show() {
		System.out.println("Hello I am show");
	}
}
