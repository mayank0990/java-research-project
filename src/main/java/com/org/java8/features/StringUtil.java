package com.org.java8.features;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class StringUtil {
	
	public static void main(String[] args) {
		String str = "[{pagePath:/content/abc,productType:credit-card,id:mayank},{pagePath:/content/abc,productType:debit-card}]";
		System.out.println(str);
		String listOfObj = str.replaceAll(":\"?([^{|^]*?)\"?(?=[,|}|\\]])",":\"$1\"").replaceAll("\"?(\\w+)\"?(?=:)","\"$1\"");
		System.out.println(listOfObj);
		Type type = new TypeToken<List<ProductCard>>() {}.getType();
		List<ProductCard> listofProducts = new GsonBuilder().serializeNulls().create().fromJson(listOfObj, type);
		System.out.println(listofProducts);
	}

}
