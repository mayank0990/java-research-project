package com.org.java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonDeserializeNullExample {
	
	public static void main(String[] args) {
		String sampleJson = "{ \"name\": \"mayank\", \"id\": \"\"}";
		Gson gson = new GsonBuilder().serializeNulls().create();
		Employee empObj = gson.fromJson(sampleJson, Employee.class);
		System.out.println(empObj);
	}
}
