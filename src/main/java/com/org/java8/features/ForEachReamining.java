package com.org.java8.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ForEachReamining {
	
	static void show(String name) {
		System.out.println("Name : " + name);
	}
	
	public static void main(String[] args) {
		List<String> strings = new ArrayList<>();
		strings.addAll(Arrays.asList("Mayank", "Hemang", "Akash", "Mehul"));
		
		
		List<String> names = new ArrayList<>();
		Iterator<String> it = strings.iterator();
		
		it.forEachRemaining(names::add);
		
		System.out.println(names);
	}

}
