package com.org.java8.features;

import org.apache.commons.io.FilenameUtils;

public class ExtractFileExtension {
	
	public static void main(String[] args) {
		String ext1 = FilenameUtils.getExtension("/path/to/file/foo.txt");
		System.out.println(ext1);
	}
}
