package com.org.java;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JSONArrayCheck {
	
	public static void main(String[] args) {
		Student st1 = new Student();
		st1.setName("mayank");
		st1.setEmail("mayank@gmail.com");
		st1.setRollNo("25");
		
		Student st2 = new Student();
		st2.setName("hemang");
		st2.setEmail("heamng@gmail.com");
		st2.setRollNo("23");
		
		List<Student> stList = new ArrayList<>();
		stList.add(st1);
		stList.add(st2);
		
		Gson gson = new Gson();
		String json = gson.toJson(stList);
		System.out.println(json);
		
		Student[] list = gson.fromJson(json, Student[].class);
		
		List<Student> students;
		Type listType = new TypeToken<List<Student>>() {}.getType();
		students = gson.fromJson(json, listType);
		System.out.println(students);
	}

}
