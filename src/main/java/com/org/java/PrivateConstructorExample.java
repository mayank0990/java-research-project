package com.org.java;

public final class PrivateConstructorExample {
	
	private PrivateConstructorExample() {
	}
	
	public static String show() {
		System.out.println("Hello");
		return "Some Text";
	}

}
