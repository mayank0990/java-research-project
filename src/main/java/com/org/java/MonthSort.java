package com.org.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

public class MonthSort {
	
	public static void main(String[] args) {
		List<String> monthsList = new ArrayList<>(Arrays.asList("March", "December", "April", "January"));
		List<String> sortedMonths = new ArrayList<>();
		
		SortedSet<String> monthSet = new TreeSet<>(new Comparator<String>() {
		    public int compare(String o1, String o2) {
		        try {
		            SimpleDateFormat fmt = new SimpleDateFormat("MMM", Locale.US );
		            return fmt.parse(o1).compareTo(fmt.parse(o2));
		        } catch (ParseException ex) {
		            return o1.compareTo(o2);
		        }
		    }
		});
		
		monthSet.addAll(monthsList);
		sortedMonths.addAll(monthSet);
		System.out.println(sortedMonths);
	}

}
