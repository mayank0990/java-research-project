package com.org.java;

import com.google.gson.Gson;

public class ShowData {
	
	public static void main(String[] args) {
		ObjectTest obj = new ObjectTest();
		Student student = obj.storeData();
		showData(student);		
	}
	
	public static void showData(final Student student) {
		Gson gson = new Gson();
		String json = gson.toJson(student);
		System.out.println(json);
	}

}
