package com.org.java8.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LambdaExpression {

	public static void main(String[] args) {
		
		List<String> strings = new ArrayList<>();
		strings.addAll(Arrays.asList("mayank", "hemang", "akash"));
		
		strings.forEach(a -> {
			System.out.println(a + "surname");
		});
		
	
	}
}
