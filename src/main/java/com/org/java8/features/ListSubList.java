package com.org.java8.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListSubList {
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.addAll(Arrays.asList("a","b","c","d","e","f","g","h"));
		System.out.println(list.subList(3, 6));
	}

}
