package com.org.reflection.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionTest {
	
	public static void main(String[] args) {
		
		try {
			Class<?> c = Class.forName("com.org.reflection.client.Person");
			Class<?>[] type = { String.class, Integer.class };
			Constructor<?> constructor = c.getConstructor(type);
			Object[] obj = {"Akash", 06};
			Object customObj = constructor.newInstance(obj);
			
			//Object obj = c.newInstance();
						 
			System.out.println("-----------------------------------");
			
			// 1. Getting all the Constructors of the class 
			Constructor<?>[] constructors =  c.getConstructors();
			System.out.println("Constructors : " + Arrays.toString(constructors));
			
			// 2. Getting all the methods (even inherited) of the class
			Method[] methods = c.getMethods();
			System.out.println("-----------------------------------");
			System.out.println("Methods : " + Arrays.asList(methods));
			
			// 3. Getting all the Declared Method of the class
			Method[] declaredMethods = c.getDeclaredMethods();
			System.out.println("-----------------------------------");
			System.out.println("Declared Methods : " + Arrays.asList(declaredMethods));
			
			// 4. Getting fields of the class
			Field[] fields = c.getDeclaredFields();
			System.out.println("-----------------------------------");
			System.out.println("Fields : "  + Arrays.toString(fields));
			
			for(Field f : fields) {				
				if(f.getName().equalsIgnoreCase("name")) {
					f.setAccessible(true);
					f.set(customObj, "Mayank");
					String str = (String) f.get(customObj);
					System.out.println("Manipulated Value : " + str);	
				}
			}
			

		} catch(ClassNotFoundException e) {
			System.out.println(e);
		} catch (SecurityException e) {
			System.out.println(e);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}
