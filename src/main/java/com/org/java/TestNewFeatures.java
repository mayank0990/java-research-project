package com.org.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TestNewFeatures {
	
	public static void main(String[] args) throws ParseException {
		String str = "Account Opening";
		String newStr = StringUtils.deleteWhitespace(str);
		String newStr1 = str.replaceAll("\\s","");
		
		System.out.println(newStr);
		
		String str1 = "aem-demo-project/components/content/sample";
		String str2 = "aem-demo-project/components/content";
		
		System.out.println(str1.contains(str2));
		
		
		String dateInString = "2019-03-15T18:30:00.000Z";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
		String formattedDate = new SimpleDateFormat("dd-MM-yyyy").format(simpleDateFormat.parse(dateInString));
		System.out.println(formattedDate);
		
		Gson gson = new Gson();
		List<Student> listofstudents = new ArrayList<>();
		Student student = new Student();
		student.setEmail("email");
		student.setName("gfg");
		student.setRollNo("dfdfg");
		
		Student student2 = new Student();
		student2.setEmail("email");
		student2.setName("gfg");
		student2.setRollNo("dfdfg");
		student2.setSubject(Arrays.asList("Maths", "Science"));
		
		listofstudents.add(student);
		listofstudents.add(student2);
		
		String json = gson.toJson(listofstudents);
		System.out.println(json);
		
	}

}
