package com.org.java8.features;

public class ProductCard {
	
	private String pagePath;
	private String productType;
	private String id;
	
	public String getPagePath() {
		return pagePath;
	}
	
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
	
	public String getProductType() {
		return productType;
	}
	
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ProductCard [pagePath=" + pagePath + ", productType=" + productType + ", id=" + id + "]";
	}
}
