package com.org.java;

import com.google.gson.annotations.SerializedName;

public class Employee {
	
	private String name;
	
	@SerializedName("Id")	
	private Integer id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", id=" + id + "]";
	}
		
}
