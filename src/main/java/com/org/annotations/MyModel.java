package com.org.annotations;

public @interface MyModel {
	
	public Class[] adaptables();

}
