package com.org.java;

abstract class Test {
	abstract void calculate(double x);
}

class A extends Test {

	@Override
	void calculate(double x) {
		System.out.println(5 * x);		
	}
	
}

class B extends Test {
	@Override
	void calculate(double x) {
		System.out.println(5 + x);		
	}
}

public class TestAbstract {
	public static void main(String[] args) {
		A a = new A();
		a.calculate(2.3);
	}
}